package co.com.proyectobase.screenplay.tasks;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

import co.com.proyectobase.screenplay.question.MensajeExitosoActivacionPrepago;
import co.com.proyectobase.screenplay.question.MessageWelcomeTigoPageMain;
import co.com.proyectobase.screenplay.ui.LogeoCRMPortalPageObject;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.waits.WaitUntil;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.*; 
import static net.serenitybdd.screenplay.questions.WebElementQuestion.the;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;

public class LogeoCRMPortal implements Task{

	private LogeoCRMPortalPageObject logeoCRMPortalPageObject;
	private String user,password;
	
	public LogeoCRMPortal(String user, String password) {
		this.user = user;
		this.password = password;
		
		
	}
	

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Enter.theValue(user).into(logeoCRMPortalPageObject.TXT_USER),
				Enter.theValue(password).into(logeoCRMPortalPageObject.TXT_PASSWORD),
				Click.on(logeoCRMPortalPageObject.BTN_LOGUEO));
		BrowseTheWeb.as(actor).getDriver().switchTo().frame("iframe");
		actor.should(seeThat(MessageWelcomeTigoPageMain.value(), equalTo("Bienvenido al portal Tigo CRM")));
		BrowseTheWeb.as(actor).getDriver().switchTo().defaultContent();
	}


	public static LogeoCRMPortal StarSession(String user2, String password2) {
		// TODO Auto-generated method stub
		return Tasks.instrumented(LogeoCRMPortal.class, user2,password2);
	}
	
	
}
