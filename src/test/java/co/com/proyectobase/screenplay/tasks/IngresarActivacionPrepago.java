package co.com.proyectobase.screenplay.tasks;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;
import static org.junit.Assert.assertEquals;

import java.nio.channels.SeekableByteChannel;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


import co.com.proyectobase.screenplay.ui.ActivacionPrepagoPageObj;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.ClickOnElement;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.actions.selectactions.SelectByVisibleTextFromBy;
import net.serenitybdd.screenplay.actions.selectactions.SelectByVisibleTextFromElement;
import net.serenitybdd.screenplay.actions.selectactions.SelectByVisibleTextFromTarget;
import net.serenitybdd.screenplay.conditions.Check;
import net.serenitybdd.screenplay.questions.SelectedVisibleTextValue;
import net.serenitybdd.screenplay.waits.WaitUntil;

public class IngresarActivacionPrepago implements Task{
	
	
private ActivacionPrepagoPageObj activacionPrepagoPageObj;
private String CC,imsi, msisdn;
 
public IngresarActivacionPrepago(String CC, String imsi, String msisdn) {

	this.CC = CC;
	this.imsi = imsi;
	this.msisdn = msisdn;
	
}

	@Override
	public <T extends Actor> void performAs(T actor) {
	
		actor.attemptsTo(Click.on(activacionPrepagoPageObj.MENUVENTA),
						 Click.on(activacionPrepagoPageObj.SUBMENUACTIVACION),	
				         Click.on(activacionPrepagoPageObj.SUBMENU2PAGOPLAZOS),
				         Click.on(activacionPrepagoPageObj.SUBMENU3ACTIVACION));
						BrowseTheWeb.as(actor).getDriver().switchTo().frame("iframe");
		actor.attemptsTo(WaitUntil.the(activacionPrepagoPageObj.BTNTIPOACTIVACION, isVisible()),	
		         		Click.on(activacionPrepagoPageObj.BTNTIPOACTIVACION),
		         		Click.on(activacionPrepagoPageObj.TIPOPREPAGO),
		         		WaitUntil.the(activacionPrepagoPageObj.TXTVENDEDOR, isVisible()),
		         		Enter.theValue("9774849").into(activacionPrepagoPageObj.TXTVENDEDOR),
		         		Click.on(activacionPrepagoPageObj.BTNTIPODOCUMENTO),
		         		Click.on(activacionPrepagoPageObj.TXTTIPODOCUMENTO),
		         		Enter.theValue(CC).into(activacionPrepagoPageObj.TXTNUMDOCUMENTO),
		         		Enter.theValue("2000").into(activacionPrepagoPageObj.TXTANOEXPEDICION),
		         		Click.on(activacionPrepagoPageObj.BTNCONTINUARINFCLIENTE),
		         		Enter.theValue("Accenture").into(activacionPrepagoPageObj.TXTNOMBRECLIENTE1),
			    		Enter.theValue("Pruebas").into(activacionPrepagoPageObj.TXTNAPELLCLIENTE),
			    		Enter.theValue(imsi).into(activacionPrepagoPageObj.TXTIMSI),
    				    Enter.theValue(msisdn).into(activacionPrepagoPageObj.TXTMSISDN),
	    				Click.on(activacionPrepagoPageObj.BTNTIPOVENTA),
	    			    Click.on(activacionPrepagoPageObj.TXTVENTASIMSOLA), 
    				    Click.on(activacionPrepagoPageObj.BTNCONTINUARACTIVACION)
    				   
    				    );
		
		BrowseTheWeb.as(actor).waitFor(30).seconds();
		actor.attemptsTo(Click.on(activacionPrepagoPageObj.BTNCONTINUARTARIFA));
						
		BrowseTheWeb.as(actor).waitFor(20).seconds();
				
		actor.attemptsTo(Click.on(activacionPrepagoPageObj.BTNDIRECCION));
		actor.attemptsTo(Click.on(By.xpath("html/body/div[5]/div/div[1]/div[1]/div[2]/select")));
		actor.attemptsTo(SelectFromOptions.byVisibleText("Avenida").from(activacionPrepagoPageObj.BTNSELECTDIREECION),
				Click.on((activacionPrepagoPageObj.BTNACEPTARDIRECCION)));
				
		actor.attemptsTo(Click.on(activacionPrepagoPageObj.BTNDEPARTAMENTO),
							Enter.theValue("Antioquia").into(activacionPrepagoPageObj.BTNDEPARTAMENTO).thenHit(Keys.ENTER),
							Click.on(activacionPrepagoPageObj.BTNMUNICIPIO),
							Enter.theValue("Medellin").into(activacionPrepagoPageObj.BTNMUNICIPIO).thenHit(Keys.ENTER),
							Enter.theValue("44454545").into(activacionPrepagoPageObj.TXTTELEFONO),
							Click.on(activacionPrepagoPageObj.BTNCONTINFDEMO),
							Click.on(activacionPrepagoPageObj.BTNCONTMODAL));
		BrowseTheWeb.as(actor).waitFor(60).seconds();

		
		
		
	
	}
	

	
	public static IngresarActivacionPrepago ActivacionPrepago(String cC2, String imsi2, String msisdn2) {
		// TODO Auto-generated method stub
		return Tasks.instrumented(IngresarActivacionPrepago.class,cC2,imsi2,msisdn2);
	}





}
