package co.com.proyectobase.screenplay.question;

import org.openqa.selenium.By;

import co.com.proyectobase.screenplay.ui.ActivacionPrepagoPageObj;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
public class MensajeExitosoActivacionPrepago implements Question<String>{

	@Override
	public String answeredBy(Actor actor) {
		
		return BrowseTheWeb.as(actor).find(By.xpath("html/body/div[1]/div/form/span/div/div[2]/table/tbody/tr[2]/td[2]/label")).getText();
		// return ActivacionPrepagoPageObj.MENSAJEACTIVACIONEXITOSA.resolveFor(actor).getText();
	}
	

    public static Question<String> value() { 
    	return new MensajeExitosoActivacionPrepago(); }
}


