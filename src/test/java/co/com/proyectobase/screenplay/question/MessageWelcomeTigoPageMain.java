package co.com.proyectobase.screenplay.question;

import org.openqa.selenium.By;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;

public class MessageWelcomeTigoPageMain implements Question<String> {
	
	
	@Override
	public String answeredBy(Actor actor) {
		
		return BrowseTheWeb.as(actor).find(By.xpath("html/body/table/tbody/tr/td/p[2]")).getText();

	}
	

    public static Question<String> value() { 
    	return new MessageWelcomeTigoPageMain(); }
}
