package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.targets.Target;

public class ActivacionPrepagoPageObj {
	
	public static final Target  MENUVENTA= Target.the("Menu ventas").located(By.xpath("//*[@id=\"UIHorizontalNavigation\"]/li[2]/span/a"));
	
	public static final Target  SUBMENUACTIVACION= Target.the("Submenu link activacion ").located(By.xpath("html/body/div[2]/div[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div[1]/div[3]/div/div/div/div/div/div/div/div[1]/div[2]/div/div/div/div/div/div/table/tbody/tr/td[1]/form/div/div/div/div/div[3]/table/tbody/tr/td/div/div[1]/table[1]/tbody/tr/td[1]/div/a/img[1]"));

	public static final Target  SUBMENU2PAGOPLAZOS= Target.the("Submenu 2 link pago a plazos").located(By.xpath("html/body/div[2]/div[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div[1]/div[3]/div/div/div/div/div/div/div/div[1]/div[2]/div/div/div/div/div/div/table/tbody/tr/td[1]/form/div/div/div/div/div[3]/table/tbody/tr/td/div/div[1]/div[1]/table[5]/tbody/tr/td[1]/div/a/img[1]"));

	public static final Target   SUBMENU3ACTIVACION = Target.the("Submenu 3 link de activador").located(By.xpath("html/body/div[2]/div[2]/div/div/div/div/div/div/div/div/div/div/div/div/div/div[1]/div[3]/div/div/div/div/div/div/div/div[1]/div[2]/div/div/div/div/div/div/table/tbody/tr/td[1]/form/div/div/div/div/div[3]/table/tbody/tr/td/div/div[1]/div[1]/div[5]/table[1]/tbody/tr/td[3]/a"));

	public static final Target  BTNTIPOACTIVACION= Target.the("Boton de tipo de activacion").located(By.xpath("html/body/form/div/div[2]/table/tbody/tr/td[2]/div/div/div[1]/span/span/span"));

	public static final Target  TIPOPREPAGO= Target.the("Opcion de menu de tipo prepago").located(By.xpath("html/body/div[2]/div/div[5]/div/div/div[1]"));

	public static final Target  TXTVENDEDOR= Target.the("Campo de texto vendedor").located(By.xpath("html/body/form[1]/div/div/div[1]/div[2]/table/tbody/tr/td[2]/div/div/input"));

	public static final Target  BTNTIPODOCUMENTO= Target.the("Boton tipo documento de cliente").located(By.xpath("html/body/form[1]/div/div/div[2]/div[2]/table/tbody/tr[1]/td[2]/div/div/div/span/span/span"));
	
	public static final Target  TXTTIPODOCUMENTO= Target.the("Texto tipo de documento cliente").located(By.xpath("html/body/div[4]/div/div[5]/div/div/div[1]"));
	
	public static final Target TXTNUMDOCUMENTO= Target.the("Canpo numero de numero de documento cliente").located(By.xpath("html/body/form[1]/div/div/div[2]/div[2]/table/tbody/tr[1]/td[5]/div/div/input"));

	public static final Target  TXTANOEXPEDICION= Target.the("Campo de numero para a�o de expedicion de documento cliente").located(By.xpath("html/body/form[1]/div/div/div[2]/div[2]/table/tbody/tr[2]/td[2]/div/div/input"));

	public static final Target  BTNCONTINUARINFCLIENTE= Target.the("Boton continuar de seccion informaci� de cliente").located(By.xpath("html/body/form[1]/div/div/div[2]/div[2]/input[1]"));

	public static final Target  TXTNOMBRECLIENTE1= Target.the("Campo de texto para nombre del cliente ").located(By.xpath("html/body/form[1]/div/div/div[3]/div[2]/table/tbody/tr/td[2]/div/div/input"));
													
	public static final Target  TXTNAPELLCLIENTE= Target.the("Campo para apellido de cliente").located(By.xpath("html/body/form[1]/div/div/div[3]/div[2]/table/tbody/tr/td[4]/div/div/input"));
		
	public static final Target  TXTIMSI= Target.the("Campo de numero para imsi del equipo").located(By.xpath("html/body/form[1]/div/div/div[6]/div[2]/table/tbody/tr[1]/td[2]/div/div/input"));
		
	public static final Target  TXTMSISDN= Target.the("Campo numero para msisdn del equipo").located(By.xpath("html/body/form[1]/div/div/div[6]/div[2]/table/tbody/tr[1]/td[5]/div/div/input"));
	//public static final Target  = Target.the("").located(By.xpath(""));
	public static final Target BTNTIPOVENTA = Target.the("Boton para seleccionar el tipo de venta prepago").located(By.xpath("html/body/form[1]/div/div/div[6]/div[2]/table/tbody/tr[2]/td[2]/div/div/div/span/span/span"));

	public static final Target  TXTVENTASIMSOLA= Target.the("Opci�n de compra de sim sola prepago").located(By.xpath("html/body/div[4]/div/div[5]/div/div/div[1]"));

	public static final Target  BTNCONTINUARACTIVACION = Target.the("Boton para continuar con la activaci�n").located(By.xpath("html/body/form[1]/div/div/div[6]/div[2]/input[1]"));

	public static final Target  BTNCONTINUARTARIFA= Target.the("Boton para continuar con la tarifa").located(By.xpath("html/body/form[1]/div/div/div[7]/div[2]/input[1]"));

	public static final Target  BTNDIRECCION= Target.the("Boton para abrir ventana emergente de direccion").located(By.xpath("html/body/form[1]/div/div/div[8]/div[2]/table/tbody/tr[1]/td/table/tbody/tr[1]/td[2]/div/div/input"));

	public static final Target  BTNSELECTDIREECION= Target.the("Seccion despliega la opcion AVENIDA").located(By.xpath("html/body/div[5]/div/div[1]/div[1]/div[2]/select"));
	
	public static final Target  BTNACEPTARDIRECCION= Target.the("Boton para aceptar direccion ingresada").located(By.xpath("html/body/div[5]/div/div[3]/div[1]/div"));

	public static final Target  BTNDEPARTAMENTO= Target.the("Boton departamento").located(By.xpath("html/body/form[1]/div/div/div[8]/div[2]/table/tbody/tr[1]/td/table/tbody/tr[2]/td[2]/div/div/div/span/input[2]"));
	
	public static final Target  BTNMUNICIPIO= Target.the("Boton municipio").located(By.xpath("html/body/form[1]/div/div/div[8]/div[2]/table/tbody/tr[1]/td/table/tbody/tr[2]/td[5]/div/div/div/span/input[2]"));

	public static final Target  TXTTELEFONO= Target.the("Campo numerico para telefono de cliente").located(By.xpath("html/body/form[1]/div/div/div[8]/div[2]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/div/div/input"));

	public static final Target  BTNCONTINFDEMO= Target.the("Boton para continuar informacion demografica").located(By.xpath("html/body/form[1]/div/div/div[8]/div[2]/input[1]"));

	public static final Target  BTNCONTMODAL= Target.the("Boton para continuar en ventana modal").located(By.xpath("html/body/form[3]/div[3]/div[2]/div/div/input[1]"));

	public static final Target  MENSAJEACTIVACIONEXITOSA= Target.the("Mensaje de activaci�n exitosa de linea prepago sin equipo").located(By.xpath("html/body/div[1]/div/form/span/div/div[2]/table/tbody/tr[2]/td[2]/label"));
	

}
