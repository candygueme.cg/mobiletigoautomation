package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class LogeoCRMPortalPageObject extends PageObject {

public static final Target TXT_USER = Target.the("Campo de texto usuario").located(By.xpath("html/body/div[1]/div[2]/form/div[1]/input"));
	
public static final Target TXT_PASSWORD = Target.the("Campo de numero de password ").located(By.xpath( "//*[@id=\"password\"]"));
	
public static final Target  BTN_LOGUEO= Target.the("Boton submit para logueo ").located(By.xpath("//*[@id=\"loginForm\"]/div[3]/input[2]"));


}
