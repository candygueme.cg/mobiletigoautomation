package co.com.proyectobase.screenplay.data;

public class Inventory {
	
	private String article;
	private String codeEAN;
	private String serial;
	

	public Inventory() {
		// TODO Auto-generated constructor stub
	}


	public String getArticle() {
		return article;
	}


	public void setArticle(String article) {
		this.article = article;
	}


	public String getCodeEAN() {
		return codeEAN;
	}


	public void setCodeEAN(String codeEAN) {
		this.codeEAN = codeEAN;
	}


	public String getSerial() {
		return serial;
	}


	public void setSerial(String serial) {
		this.serial = serial;
	}

}
