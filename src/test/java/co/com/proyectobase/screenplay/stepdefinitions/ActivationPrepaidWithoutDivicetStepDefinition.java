package co.com.proyectobase.screenplay.stepdefinitions;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.tasks.LogeoCRMPortal;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Managed;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.*; 
import static net.serenitybdd.screenplay.questions.WebElementQuestion.the;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;
public class ActivationPrepaidWithoutDivicetStepDefinition {
	
	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor juan = Actor.named("Juan");
	
	@Before
	public void configuracionInicial() {
	juan.can(BrowseTheWeb.with(hisBrowser));
	}
	

	@Given("^Juan can login the page web CRM PORTAL with \"([^\"]*)\" and \"([^\"]*)\"$")
	public void juanCanLoginThePageWebCRMPORTALWithAnd(String user, String password) throws Exception {
		juan.wasAbleTo(Open.url("http://10.69.60.84:8280/portal/CRMPortal/Postventa"));
		juan.wasAbleTo(LogeoCRMPortal.StarSession(user,password));
	}


	@Given("^Enter module Activador$")
	public void enter_module_Activador() throws Exception {
	   
	}

	@When("^Enter the activation Prepago$")
	public void enter_the_activation_Prepago() throws Exception {
	  
	}

	@When("^Enter in the section Information agent (\\d+)$")
	public void enter_in_the_section_Information_agent(int arg1) throws Exception {
	    
	}

	@When("^Enter in the section Customer CC (\\d+) (\\d+)$")
	public void enter_in_the_section_Customer_CC(int arg1, int arg2) throws Exception {
	   
	}

	@When("^Enter in the section Information additional of Customer Juana Gutierrez$")
	public void enter_in_the_section_Information_additional_of_Customer_Juana_Gutierrez() throws Exception {
	   
	}

	@When("^Enter in the section Information Activation (\\d+) (\\d+) Sin equipo$")
	public void enter_in_the_section_Information_Activation_Sin_equipo(int arg1, int arg2) throws Exception {
	   
	}

	@When("^Accept the section Tarifa$")
	public void accept_the_section_Tarifa() throws Exception {
	    
	}

	@When("^Enter in the section Information Demograpic AvenidaBogotaBogota(\\d+)$")
	public void enter_in_the_section_Information_Demograpic_AvenidaBogotaBogota(int arg1) throws Exception {
	    
	}

	@Then("^Verify menssage Activation Successful$")
	public void verify_menssage_Activation_Successful() throws Exception {
	   
	}
	
	
}
