package co.com.proyectobase.screenplay.stepdefinitions;

import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.data.UserEPOS;
import co.com.proyectobase.screenplay.tasks.LogeoCRMPortal;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Managed;

public class AddInventoryEPOSStepDefinitions {
	
	
	
	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor actor = Actor.named("Juan");
	private UserEPOS userEPOS = new UserEPOS();
	
	@Before
	public void configuracionInicial() {
		actor.can(BrowseTheWeb.with(hisBrowser));
	}
		@Given("^Init session on EPOS$")
		public void initSessionOnCRM() throws Exception {
			
			actor.wasAbleTo(Open.url("http://10.69.60.75:8180/tigo-pos-web/pages/puntoVenta/index.jsf?param=1"));
			actor.wasAbleTo(LogeoCRMPortal.StarSession(userEPOS.getUser(),userEPOS.getPassword()));	
		}


		@When("^Add device$")
		public void addDevice() throws Exception {
	
		}

		@When("^asociate device with user agent$")
		public void asociateDeviceWithUserAgent() throws Exception {
	
		}

		@Then("^Validate asociation$")
		public void validateAsociation() throws Exception {
	
		}
}
