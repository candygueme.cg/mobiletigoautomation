#Author: candy.guerrer@accenture.com

@tag
Feature: Title of your feature
  I want to use this template for my feature file

#Activacion prepago sin equipo con cedula no existen
  @ActPreSnCCEx

  Scenario Outline: Activation Pre-paid witchout equipment with ID card not exit.
    Given Juan can login the page web CRM PORTAL with <user> and <password> 
    And  Enter module Activador 
    When Enter the activation <Activation>
    And  Enter in the section Information agent <IDAgent>
    And  Enter in the section Customer <TypeID> <NumerID> <YearExpedition>
    And  Enter in the section Information additional of Customer <name> <lastname>
    And  Enter in the section Information Activation <Imsi> <Msisdn> <TypeActivation>
    And  Accept the section Tarifa
    And  Enter in the section Information Demograpic <Direcciotion><Department><City><Telephone>
    Then Verify menssage Activation Successful

    Examples: 
      | user       | password | Activation  |IDAgent|TypeID|NumerID   |YearExpedition|name |lastname |Imsi           |Msisdn    |TypeActivation|Direcciotion|Department|City  |Telephone|
      | "PA9774849" | "Tigo.2019*" | Prepago 		 |9774849|CC    |1904564590|2000          |Juana|Gutierrez|732111178440808|3015579461|Sin equipo    |Avenida     |Bogota    |Bogota|23456677 |
      
      
      
        @ActPreSnNITEx

  Scenario Outline: Activation Pre-paid witchout equipment with NIT not exit.
    Given Juan can login the page web CRM PORTAL with <user> and <password> 
    And  Enter module Activador 
    When Enter the activation <Activation>
    And  Enter in the section Information agent <IDAgent>
    And  Enter in the section Customer <TypeID> <NumerID> <YearExpedition>
    And  Enter in the section Information additional of Customer <name> <lastname>
    And  Enter in the section Information Activation <Imsi> <Msisdn> <TypeActivation>
    And  Accept the section Tarifa
    And  Enter in the section Information Demograpic <Direcciotion><Department><City><Telephone>
    Then Verify menssage Activation Successful

    Examples: 
      | user       | password | Activation  |IDAgent|TypeID|NumerID   |YearExpedition|name |lastname |Imsi           |Msisdn    |TypeActivation|Direcciotion|Department|City  |Telephone|
      | PA9774849| Tigo.2019* | Prepago 		 |9774849|NIT    |1904564590|2000          |Juana|Gutierrez|732111178440808|3015579461|Sin equipo    |Avenida     |Bogota    |Bogota|23456677 |
      
      
      
